# my_youtube_channel
I plan to start to record some YouTube videos in 2024.
This repo will be the place where I deposit the artifacts
of video preparation.

For the moment, I plan to include contents on
- Computer Science/Math
- Daily life (e.g. in Vietnam)


## License
MIT
