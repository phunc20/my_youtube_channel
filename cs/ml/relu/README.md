# Differential Calculus with Python


## Steps of Presentation
- Part 1
    1. $`\text{ReLU}'(0) =\,?`$
        - Reminder: `notebooks/parts/1/activation_fn_plot.jl`
        - Two immediate opinions
            1. This is serious! With elementary calculus and
               standard gradient descent, we do not know
               how to deal with $`\text{ReLU}'(0)`$.
            2. It's just a single point. Mathematically, the probability
               for a neuron to fall right into this point is zero.
               (To be developped in Part 3.)
    1. Subgradient methods.  
       Cf. e.g.
        - Steven Boyd
            - <https://web.stanford.edu/class/ee364b/lectures/subgrad_method_notes.pdf>
            - <https://www.youtube.com/watch?v=U3lJAObbMFI&t=1291s>
        - Lieven Vandenberghe
            - <http://www.seas.ucla.edu/~vandenbe/ee236c.html>
    1. How does PyTorch deal with $`\text{ReLU}'(0)`$?
        - [Gradients for non-differentiable functions](https://pytorch.org/docs/stable/notes/autograd.html#gradients-for-non-differentiable-functions)
            - Verify the stated principles in Colab (`notebooks/parts/1/pytorch_non_differentiable.ipynb`)
            - So PyTorch developers implement a convexity check
              to be applied at all non-differentiable points?
                - Source code
                    - [How to Read PyTorch's `tools/autograd/derivatives.yaml`?](#how-to-read-pytorch's-tools/autograd/derivatives.yaml?)
                    - `conj()`? Complex independent variable?
                        - Not very useful as of 2023, unless you are doing
                          complex differentiation with audio signals.
                        - "But I would still like to understand!" (To be developped in Part 2.)
                    - Hard to read? Fun fact:
                      [Andrej Karpathy found it so, too](https://www.youtube.com/watch?v=VMj-3S1tku0&t=8470s).
                        - So if you cannot seem to find something quickly, try to
                          search or ask on <https://discuss.pytorch.org/> instead.
                        - Me too, if it were not thanks to
                          [the help of albanD](https://discuss.pytorch.org/t/gradient-of-relu-at-0/64345/6)
            - `backward()` could have input arg(s)
    1. Conclusion
        - PyTorch developers are aware of subgradient methods. (Of course!)
        - But the usual way in which one trains neural networks ignores
          subgradient methods.
        - This does not mean that PyTorch is incapable of subgradient methods.
        - The Autograd Mechanics article describes only general principles
          for both framework developers and end-user developers to follow.
          The exact implementation might differ (e.g. in the leaky ReLU case
          we inspected above) from the principles.
- Part 2
    1. Define new autograd functions in PyTorch
        1. Through Python
            - Elaborate on [the jupyter notebook (on PyTorch's autograd) from an IBM employee with an EE PhD](https://gist.github.com/ArashRasti/b82737d429b3a3e049259ebf5defab1a)
                - To prevent from the deletion of the GitHub Gist, we have also
                  backed it up in this repo
                  (at `notebooks/parts/2/1.2derivativesandGraphsinPytorch_v2.ipynb`).
                - Three-stage correction
                    1. The original
                    1. Tak `grad_output` into consideration
                    1. Take `conj()` into consideration
            - Mention a few further readings
                - <https://pytorch.org/tutorials/beginner/examples_autograd/two_layer_net_custom_function.html>
                - <https://pytorch.org/docs/stable/autograd.html#function>
                - <https://pytorch.org/docs/stable/notes/extending.html#extending-autograd>
        1. Through `tools/autograd/derivatives.yaml` (To be explored later)
        1. [As a C++ extension](https://pytorch.org/tutorials/advanced/cpp_extension.html#motivation-and-example)
    1. Wirtinger Calculus
        - [Autograd Mechanics' section on Wirtinger calculus (PyTorch)](https://pytorch.org/docs/stable/notes/autograd.html#autograd-for-complex-numbers)
            - If one doesn't search to understand, then just apply equation (4).
            - The discussion here is **not very rigorous** -- I am unable to
              fully understand it during the first lecture.
            - Demo
                - PyTorch's complex gradient: $`\frac{\partial{L}}{\partial{\bar{z}}}`$
                - JAX's complex gradient: $`\frac{\partial{L}}{\partial{z}}`$
        - Mention my own `wirtinger_calculus.ipynb`
        - A little too theoretic, describe only the most important idea/perspective:
          Think about doing **real-number** differentiation instead of complex differentiation
        - <https://github.com/pytorch/pytorch/issues/41857>
        - Ref.
            - [A short tutorial on Wirtinger Calculus with applications in quantum information](https://arxiv.org/pdf/2312.04858.pdf)
            - [Theory of Complex Functions, Reinhold Remmert](https://www.matem.unam.mx/~hector/Remmert-TheoryCpxFtns.pdf)
            - [The Autodiff Cookbook's section on complex differentiation (JAX)](https://jax.readthedocs.io/en/latest/notebooks/autodiff_cookbook.html#complex-numbers-and-differentiation)
    1. Conclusion
- Part 3
    1. Examples which might look astonishing at first glance
    1. Numerical influence of ReLU'(0) on backpropagation
        - <https://arxiv.org/abs/2106.12915>
            - I find some of the materials in this paper are not easy to understand;
              probably not because they are particularly hard, but because not
              very well expressed. E.g. bifurcation zone.
        - <https://github.com/deel-ai/relu-prime>
            - This repo is quite messy and not readily reproducible.
              Needs some modifications to reproduce the plots in the paper.
            - The paper's developers are good at PyTorch, actually
            - <https://pytorch.org/docs/stable/notes/extending.html>


## Viewpoints
- It seems that, in the past, i.e. before ReLU, when people were still using
  sigmoid or tanh, it was a common practice to precede the training
  of deep supervised neural networks with unsupervised pre-training.
  Cf. <https://en.wikipedia.org/wiki/Rectifier_(neural_networks)#Advantages>
- The softplus or SmoothReLU function is indeed a smooth version of ReLU:
  Not only the function itself resembles ReLU, but also its derivative, which
  is a smooth version of the Heaviside step function, derivative of ReLU.
- Autodiff libraries like PyTorch seem to be aware of subgradient methods.
  However, they do not completely take the path of subgradient methods; instead,
  they simply pick a subgradient for non-differentiable point and proceed
  the gradient way.
- Some curious namings
    - ATen: Probably refers to "arbitrary tensor algebra".
        - Or simply, A TENsor library (<https://github.com/zdevito/ATen>)


## TODOs
1. [ ] Andrej Karpathy's "The spelled-out intro to neural networks and backpropagation - building micrograd"
    - [A similar hands-on Colab notebook by Zachary DeVito](https://colab.research.google.com/drive/1VpeE6UvEPRz9HmsHh1KS0XxXjYu533EC)
    - <https://pytorch.org/tutorials/beginner/examples_autograd/two_layer_net_custom_function.html>
1. [ ] Re-read [your own 2021 article on backprop](https://github.com/phunc20/DL/blob/main/backprop/en_nn_andrew_ng.pdf).
1. [ ] Read [heiner.ai on autograd](https://heiner.ai/blog/2023/02/19/chain-rule-jacobians-autograd-shapes.html)
1. [ ] Read <https://github.com/pytorch/pytorch/issues/41857>
1. ATen and C++ (PyTorch)
    - [PyTorch C++ API](https://pytorch.org/cppdocs/)
    - [The C++ Frontend](https://pytorch.org/cppdocs/frontend.html)
    - [Tensor Basics](https://pytorch.org/cppdocs/notes/tensor_basics.html)
    - source code
        - `tools/autograd/derivatives.yaml`
        - `aten/src/ATen/native/Activation.cpp`
        - `aten/src/ATen/native/cpu/Activation.cpp`, e.g. `leaky_relu_backward_kernel`
1. [ ] Succeed in markdown [cross-ref](https://stackoverflow.com/questions/2822089/how-to-link-to-part-of-the-same-document-in-markdown)


## Q&A
1. How PyTorch implements derivatives at `x=0` of functions like `relu` and `abs`?
    - I really don't know (although I have made some effort searching through
      the source code of PyTorch.)
    - Ask the audience!
1. Why leaky ReLU's left slope has to default to a small value, e.g. 0.01 (PyTorch)?
    - Put in another way, is it harmful to have a big left slope?
    - Keras defaults to 0.3 (Cf. <https://keras.io/api/layers/activation_layers/leaky_relu/>)
1. For simple NNs involving only matrix multiplication and a finite number of
   activation functions, wouldn't symbolic differentiation faster than autodiff?
    - Probably no. Not really faster because, e.g., sparsity, many zero entries
      in some cases.
1. (Wirtinger) Complex derivative?
    - PyTorch, TensorFlow: $`\frac{\partial{L}}{\partial{\bar{z}}}`$
    - JAX: $`\frac{\partial{L}}{\partial{z}}`$
1. <https://pytorch.org/docs/stable/notes/autograd.html>
    - Saved tensors:
      > "the tensor you get from accessing `y.grad_fn._saved_result`
      > is a different tensor object than `y` (but they still share the same storage)."
      
      How to make different objects share the same storage generally in Python?
1. Why ReLU-like functions are all convex?
    - Couldn't we have a Leaky ReLU with left slope equal to, say, 3.0?
    - Couldn't we use the absolute value function as activation function?


## Topics
- Complex differentiation
    - [Zygote](https://fluxml.ai/Zygote.jl/stable/complex/)
    - [PyTorch](https://pytorch.org/docs/stable/notes/autograd.html#complex-autograd-doc)
    - [JAX](https://jax.readthedocs.io/en/latest/notebooks/autodiff_cookbook.html#complex-numbers-and-differentiation)
    - []()
    - []()
    - `grad.conj()` will appear for functions that do not satisfy Cauchy-Riemann
      equations, e.g.
      ```bash
      $ grep -n -B2 "grad.conj" tools/autograd/derivatives.yaml
      463-
      464-- name: _conj(Tensor(a) self) -> Tensor(a)
      465:  self: grad.conj()
      --
      471-
      472-- name: _conj_physical(Tensor self) -> Tensor
      473:  self: grad.conj_physical()
      --
      475-
      476-- name: conj_physical_(Tensor(a!) self) -> Tensor(a!)
      477:  self: grad.conj_physical()
      --
      597-
      598-- name: vdot(Tensor self, Tensor other) -> Tensor
      599:  self: grad.conj() * other
      --
      2615-- name: sigmoid_backward(Tensor grad_output, Tensor output) -> Tensor
      2616-  grad_output: sigmoid_backward(grad, output.conj())
      2617:  output: grad.conj() * grad_output * (-2 * output.conj() + 1)
      --
      2620-- name: tanh_backward(Tensor grad_output, Tensor output) -> Tensor
      2621-  grad_output: tanh_backward(grad, output.conj())
      2622:  output: grad.conj() * (-2 * output.conj() * grad_output)
      ```
- How is `derivatives.yaml` used in the source code?
    - `tools/autograd/gen_autograd.py` (found by `grep -Rn "derivatives.yaml\""`)
- Manually define derivatives in PyTorch
    - <https://pytorch.org/tutorials/beginner/examples_autograd/two_layer_net_custom_function.html>
        - <https://gist.github.com/ArashRasti/b82737d429b3a3e049259ebf5defab1a>
    - <https://pytorch.org/docs/stable/autograd.html#function>
    - <https://pytorch.org/docs/stable/notes/extending.html#extending-autograd>
- [`retain_grad()` on non-leaf tensor](github.com/pytorch/pytorch/pull/30531)


## Principles of Presentation
1. Make the clips short. Each video clip should not exceed 20 minutes.
1. Remember to make a YouTube thumbnail for each clip.


## How to Read PyTorch's `tools/autograd/derivatives.yaml`?
- Let's start from the simplest: `sin`, `cos` and `exp`$
  ```yaml
  - name: sin(Tensor self) -> Tensor
    self: grad * self.cos().conj()
    result: auto_element_wise

  - name: cos(Tensor self) -> Tensor
    self: grad * -self.sin().conj()
    result: auto_element_wise

  - name: exp(Tensor self) -> Tensor
    self: grad * result.conj()
    result: auto_element_wise
  ```
- `threshold` and `relu` have similar definitions for their derivatives
  ```yaml
  - name: threshold(Tensor self, Scalar threshold, Scalar value) -> Tensor
    self: threshold_backward(grad, self, threshold)
    result: auto_element_wise
  
  - name: relu(Tensor self) -> Tensor
    self: threshold_backward(grad, result, 0)
    result: auto_element_wise
  ```
- As far as I could decrypt PyTorch's source code, PyTorch defines
  ```math
  \text{ReLU}'(x) = \begin{cases}
      0 \quad\text{if}\quad x \le 0 \\
      1 \quad\text{otherwise}
  \end{cases}
  ```
    - `aten/src/ATen/native/Activation.cpp`. The comment before `threshold_backward`
      simply computes `result = self <= threshold ? 0 : grad` and
      `relu(const Tensor& self)` could be defined via `threshold(self, 0, 0)`
      (i.e. both `threshold` and `value` equal `0`)
      ```
      // computes `result = self <= threshold ? value : other`
      // other is `self` in threshold() and `grad` in threshold_backward()
      TORCH_META_FUNC(threshold)(const Tensor& self, const Scalar& threshold, const Scalar& value) {
        const Tensor& result = maybe_get_output();
        build(TensorIteratorConfig()
          .set_check_mem_overlap(false)  // threshold is idempotent, so overlap is okay
          .add_output(result)
          .add_input(self)
          .add_input(self) // other
          .allow_cpu_scalars(true)
          .promote_inputs_to_common_dtype(true)
          .cast_common_dtype_to_outputs(true)
          .enforce_safe_casting_to_output(true));
      }
      // computes `result = self <= threshold ? value : other`
      // other is `self` in threshold() and `grad` in threshold_backward()
      TORCH_META_FUNC(threshold_backward)(const Tensor& grad, const Tensor& self, const Scalar& threshold) {
        const Tensor& gradInput = maybe_get_output();
        build(TensorIteratorConfig()
          .set_check_mem_overlap(false)  // threshold is idempotent, so overlap is okay
          .add_output(gradInput)
          .add_input(self)
          .add_input(grad)  // other
          .allow_cpu_scalars(true)
          .promote_inputs_to_common_dtype(true)
          .cast_common_dtype_to_outputs(true)
          .enforce_safe_casting_to_output(true));
      }
      ```
- The absolute value function
  ```yaml
  - name: abs(Tensor self) -> Tensor
    self: grad * self.sgn()
    result: handle_r_to_c(result.scalar_type(), self_t.conj() * self_p.sgn())
  ```
  `aten/src/ATen/native/cpu/zmath.h`
  ```
  template<typename T>
  inline c10::complex<T> sgn_impl (c10::complex<T> z) {
    if (z == c10::complex<T>(0, 0)) {
      return c10::complex<T>(0, 0);
    } else {
      return z / zabs(z);
    }
  }
  ```
  Put into mathematical notations, the above code in the real number case
  is equiv. to
  ```math
  {\left|\,\cdot\,\right|}'(x) = \begin{cases}
      &\phantom{-}0 \quad\text{if}\quad x = 0 \\
      &\phantom{-}1 \quad\text{if}\quad x \gt 0 \\
      &-1 \quad\text{if}\quad x \lt 0
  \end{cases}
  ```
