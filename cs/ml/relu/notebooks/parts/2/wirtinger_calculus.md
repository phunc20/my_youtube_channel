# The Bigger Questions
The bigger Questions that this note tries to
answer are the following.

> _Why can we differentiate any function $f(z)$, in Wirtinger Calculus,  
> w.r.t. $z$ and $\bar{z}$ as if $z$ and $\bar{z}$ were independent variables?   
> Moreover, why would such operation interest us? What's the motivation?_


# Attempts to Answer
## First The Motivation
One motivation is **simple**:  

> We want to be able to calculate the gradient
> ```math
> \begin{pmatrix} \frac{\partial f}{\partial x} \\ \frac{\partial f}{\partial y} \end{pmatrix}
> ```
> of a real-differentiable $f: \mathbb{C} \to \mathbb{R}$
> without having to go through _the hassle of expressing $z$ in terms of $x + iy$_.


## Prerequisites
> $\mathbb{C}$ is just $\mathbb{R}^{2}$ endowed
> with the multiplication
> ```math
> \begin{pmatrix} a \\ b \end{pmatrix} \begin{pmatrix} c \\ d \end{pmatrix} := \begin{pmatrix} ac - bd \\ ad + bc \end{pmatrix}\,.
> ```

For convenience's sake, we will constantly shift btw these
two viewpoints.

For instance, for a function $f: \mathbb{C} \to \mathbb{C}$, we may sometimes write

```math
f(x,y) = P(x, y) + iQ(x,y)
```

or

```math
f(x,y) = \begin{pmatrix}P(x, y) \\ Q(x,y)\end{pmatrix}
```

where $x,y,P,Q$ are understood as obtained by the standard basis of $\mathbb{R}^{2}$.


## Validity
For any real-differentiable $f: \mathbb{C} \to \mathbb{C}$,
there exists a way to calculuate the Jacobian of $f$ (w.r.t. $x$ and $y$)
by staying purely in the realm of $z$ (and $\bar{z}$).

And this section is meant to explain that.


**Attempted Proof.**  
Let $f: \mathbb{C} \to \mathbb{C}$ be real-differentiable.  
Consider the following mappings:

```math
\begin{align*}
\mathbb{R}^{2} &\stackrel{\alpha}{\to} \mathbb{C}^{2} &&\stackrel{g}{\to} \mathbb{C} \\
(x, y) &\mapsto (z, \bar{z}) &&\mapsto f\left(\frac{z+\bar{z}}{2}, \frac{z-\bar{z}}{2i}\right)\,.
\end{align*}
```

Since we define $\alpha$ as

```math
\begin{align*}
z &= x + iy \\
\bar{z} &= x - iy
\end{align*}\;,
```

we see that

```math
g \circ \alpha = f\;.
```

We want to prove that we can differentiate w.r.t. $z$ and $\bar{z}$ by treating them
as independent variables.
In particular, we want to prove that

```math
\begin{align*}
\frac{\partial f}{\partial x} &= \frac{\partial \left(g \circ \alpha \right)}{\partial x}
  = \frac{\partial g}{\partial z} \frac{\partial z}{\partial x}
    + \frac{\partial g}{\partial \bar{z}} \frac{\partial \bar{z}}{\partial x} \\
\frac{\partial f}{\partial y} &= \frac{\partial \left(g \circ \alpha \right)}{\partial y}
  = \frac{\partial g}{\partial z} \frac{\partial z}{\partial y}
    + \frac{\partial g}{\partial \bar{z}} \frac{\partial \bar{z}}{\partial y}
\end{align*} \;.
```

Associated with $g$ is this function

```math
\begin{align*}
\tilde{g}: \mathbb{R}^{4} &\to \mathbb{R}^{2} \\
   \begin{pmatrix}
       t_{1} \\
       t_{2} \\
       t_{3} \\
       t_{4}
   \end{pmatrix}
   &\mapsto
   g\left(\begin{pmatrix}t_{1}\\t_{2}\end{pmatrix}, \begin{pmatrix}t_{3}\\t_{4}\end{pmatrix}\right)\,.
\end{align*}
```

**Rmk.**  
> I have somewhat abused the notation above:  
> $g: \mathbb{C}^{2} \to \mathbb{C}$ and yet I wrote
> $g \left( \begin{pmatrix}t_{1} \\ t_{2} \end{pmatrix}, \begin{pmatrix}t_{3} \\ t_{4} \end{pmatrix}\right)$.
> 
> However, recall the equivalence of $\mathbb{C}$ and $\mathbb{R}^{2}$  
> (as we have defined a multiplication of vectors in $\mathbb{R}^{2}$).  
> Such an abuse of notation can be tolerated as long as
> $g$ involves only _elementary_ operations.

Denoting

```math
\begin{align*}
\tilde{\alpha}: \mathbb{R}^{2} &\to \mathbb{R}^{4} \\
                (x, y) &\mapsto \begin{pmatrix} x \\ y \\x \\ -y \end{pmatrix}\,,
\end{align*}
```

we see that

```math
f = g \circ \alpha = \tilde{g} \circ \tilde{\alpha}\,.
```

What I believe to be true (without being able to provide more evidence than a few examples for the moment) is the following

```math
J \tilde{g} =
\left(
\begin{array}{c:c}
    \frac{\partial g}{\partial z} &
    \frac{\partial g}{\partial \bar{z}}
\end{array}
\right) \tag{1}
```

where $J \tilde{g}$ denotes the Jacobian of $\tilde{g}$.


**Example 1.**  
```math
f(z) = |z|^{2}
```

In other words,

```math
\begin{align*}
&f: \mathbb{C} \to \mathbb{C} \\
&f(x, y) = x^{2} + y^{2}\,.
\end{align*}
```

Consequently,

```math
\begin{align*}
g(z, \bar{z})
    &= f\left(\frac{z+\bar{z}}{2}, \frac{z-\bar{z}}{2i}\right)
    = \frac{\left(z+\bar{z}\right)^{2}}{4} - \frac{\left(z-\bar{z}\right)^{2}}{4}
    = z\bar{z} \\
\tilde{g}(t_{1}, t_{2}, t_{3}, t_{4})
    &= (t_{1}t_{3} - t_{2}t_{4}, t_{1}t_{4} + t_{2}t_{3})\,.
\end{align*}
```


Then
```math
J \tilde{g} = \begin{pmatrix}
  \phantom{-}t_{3} & -t_{4} & \phantom{-}t_{1} & -t_{2} \\
  \phantom{-}t_{4} & \phantom{-}t_{3} & \phantom{-}t_{2} & \phantom{-}t_{1}
\end{pmatrix}
```

and
```math
\begin{align*}
  \frac{\partial g}{\partial z} &= \bar{z} \\
  \frac{\partial g}{\partial \bar{z}} &= z\,.
\end{align*}
```

But what does the RHS of $(1)$,
i.e.

```math
\left(
\begin{array}{c:c}
    \frac{\partial g}{\partial z} &
    \frac{\partial g}{\partial \bar{z}}
\end{array}
\right)
```

mean?

Well, for those submatrices to form together the bigger $2$ by $4$ matrix
$J\tilde{g}$, it's plausible to guess that each one of $\frac{\partial g}{\partial z}$ and $\frac{\partial g}{\partial \bar{z}}$ should be a $2$ by $2$ matrix.

But $\frac{\partial g}{\partial z}$ and $\frac{\partial g}{\partial \bar{z}}$ are
complex numbers!


> What remains is the question how one views a complex number as a matrix.

Note that, further down on the way to the overall Jacobian/gradient,
$\frac{\partial g}{\partial z}$ and $\frac{\partial g}{\partial \bar{z}}$ will
be multiplied by complex numbers,
such as $\frac{\partial z}{\partial y} = i, \frac{\partial \bar{z}}{\partial y} = -i$, etc.  
The latter being able to be viewed as vectors in $\mathbb{R}^{2}$, it is thus
another supporting opinion that
$\frac{\partial g}{\partial z}$
and
$\frac{\partial g}{\partial \bar{z}}$
should be $2$ by $2$ matrices.

Algebraically, the multiplication of two complex numbers

```math
(a+ib)(c + id)
```

can be easily viewed as a matrix-vector product
```math
\begin{pmatrix}
  a & -b \\
  b & \phantom{-}a
\end{pmatrix}
\begin{pmatrix}
  c \\
  d
\end{pmatrix}\,.
```

Geometrically, multiplication by a complex number $a + ib = r e^{i\theta}$ is just

- scaling (by the amount of $r$)
- rotation (by the amonut of $\theta$)

which are **doable** via **a single matrix**

```math
r\begin{pmatrix}
  \cos\theta & -\sin\theta \\
  \sin\theta & \phantom{-}\cos\theta
\end{pmatrix}
= \begin{pmatrix}
  a & -b \\
  b & \phantom{-}a
\end{pmatrix}\,.
```

Once the road cleared, we see

```math
\left(
\begin{array}{c:c}
    \frac{\partial g}{\partial z} &
    \frac{\partial g}{\partial \bar{z}}
\end{array}
\right) = \left(
\begin{array}{cc:cc}
    t_{3} & -t_{4}           & t_{1} & -t_{2} \\
    t_{4} & \phantom{-}t_{3} & t_{2} & \phantom{-}t_{1}
\end{array}
\right) = J\tilde{g}\,.
```

**Rmk.**  
The (real) matrix representation of complex numbers respects
addition, multiplication, identity, inverse (of complex numbers):
- ```math
  \det \begin{pmatrix}a & -b \\ b & \phantom{-}a\end{pmatrix} = a^{2} + b^{2} = 0 \iff a=0 \; \text{and} \; b=0
  ```
- ```math
  \begin{pmatrix}a & -b \\ b & \phantom{-}a\end{pmatrix}^{-1} \;= \frac{1}{a^2+b^2}\begin{pmatrix}\phantom{-}a & b \\ -b & a\end{pmatrix}
  ```


**Example 2.**  
```math
\begin{align*}
f(x, y) &= e^{x-iy} \\
g(z, \bar{z}) &= e^{\bar{z}} \\
\tilde{g}(t_1, t_2, t_3, t_4) &= \begin{pmatrix}
e^{t_3}\cos t_4 \\
e^{t_3}\sin t_4
\end{pmatrix}
\end{align*}
```

Differentiating $g$ w.r.t. $z$ and $\bar{z}$ as if they were independent gives

```math
\frac{\partial g}{\partial z} = 0,\, \frac{\partial g}{\partial \bar{z}} = e^{\bar{z}}\,.
```

By writing

```math
e^{\bar{z}} = e^{t_3 + i t_4} = e^{t_3}\left(\cos t_4 + i \sin t_4\right)\,,
```

we see

```math
\left(
\begin{array}{c:c}
    \frac{\partial g}{\partial z} &
    \frac{\partial g}{\partial \bar{z}}
\end{array}
\right) = \left(
\begin{array}{cc:cc}
0 & 0 & e^{t_{3}}\cos t_{4} & -e^{t_{3}} \sin t_{4} \\
0 & 0 & e^{t_{3}}\sin t_{4} & \phantom{-}e^{t_{3}} \cos t_{4}
\end{array}\right)
= J\tilde{g}\,.
```
